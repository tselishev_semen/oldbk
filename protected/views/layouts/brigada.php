<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <title><?php  echo CHtml::encode($this->pageTitle); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
</head>
<body>
<?php echo $content; ?>
</body>
</html>
