<?php
/* @var $this RuinsController */
?>
<link
    href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css"
    rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css"
      rel="stylesheet">
<link href="/oldbk/css/ruins.css" rel="stylesheet">
<script src="/oldbk/js/ruins.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src='/oldbk/js/jquery.ui-contextmenu.js'></script>

<menu>
    <div class="ui-widget">
        <label for="location-search">Локация: </label>
        <input id="location-search" placeholder="Выберите локацию"/>
    </div>
</menu>
<div id='logo'>
   <a  href='http://oldbrigada.ru/'> <img src='//oldbrigada.ru/favicon.ico'/></a>
</div>
<section id='ruins-map'>

</section>
    <footer>
        <fieldset>
            <legend>Легенда</legend>
            <p><i class="icon-male legend-icon" style="color:#000000"></i>
                <span class="legend-text">Противник в путах</span>
            <p><i class="icon-lock legend-icon" style="color:orangered"></i>
                <span class="legend-text">В локации ловушка</span>
            <p><i class="icon-tint legend-icon" style="color:red"></i>
                <span class="legend-text">В локации идет бой</span>
        </fieldset>
    </footer>
<script>
    $(document).ready(function () {
        $('a').on('click', function () {
            $('.selected').removeClass('selected')
            var container = $(this).closest('[data-x]')
            var x = container.attr('data-x')
            var y = container.attr('data-y')
            $('.header-select').removeClass('header-select')
            $("[data-x='0'][data-y=" + y + "]").addClass('header-select')
            $("[data-x=" + x + "][data-y='0']").addClass('header-select')
            $(this).addClass('selected')
        })

        $(".center").contextmenu({
            menu: [
                {title: "Противник в путах", cmd: 'stop'},
                {title: "Ловушка", cmd: "trap"},
                {title: "Идет бой", cmd: "battle"}
            ],
         show: { effect: "blind", duration: 0 },
            select: function (event, ui) {
                var image = $(this).find('.' + ui.cmd)
                switch (ui.cmd) {
                    case 'stop':
                        var icon = 'icon-male'
                        break;
                    case 'trap':
                        var icon = 'icon-lock';
                        break;
                    case 'battle':
                        var icon = 'icon-tint'
                        break;
                    default:
                        var icon = ''
                        break;
                }
                if (image.hasClass(icon)) {
                    image.removeClass(icon)
                } else {
                    image.addClass(icon)
                }
            }
        });
    })

</script>

