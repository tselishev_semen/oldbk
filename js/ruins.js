$(document).ready(function () {
    var locationsObj = {
        "Черная башня": {x: 1, y: 1, right: true, left: false, top: false, bottom: false},
        "Северные чертоги": {x: 2, y: 1, right: true, left: true, top: false, bottom: true},
        "Разрушенная Северная башня": {x: 3, y: 1, right: true, left: true, top: false, bottom: true},
        "Высохшее водохранилище": {x: 4, y: 1, right: true, left: true, top: false, bottom: false},
        "Северный обрыв": {x: 5, y: 1, right: true, left: true, top: false, bottom: true},
        "Таинственное логово": {x: 6, y: 1, right: true, left: true, top: false, bottom: false},
        "Угрюмый лес": {x: 7, y: 1, right: true, left: true, top: false, bottom: false},
        "Сгоревший частокол": {x: 8, y: 1, right: true, left: true, top: false, bottom: true},
        "Гигантская нора": {x: 9, y: 1, right: true, left: true, top: false, bottom: false},
        "Непроходимый бурьян": {x: 10, y: 1, right: true, left: true, top: false, bottom: true},
        "Часовня темных побуждений": {x: 11, y: 1, right: true, left: true, top: false, bottom: true},
        "Красный трон": {x: 12, y: 1, right: false, left: true, top: false, bottom: false},
        "Западныый склон": {x: 2, y: 2, right: true, left: false, top: true, bottom: true},
        "Подземелье смерти": {x: 3, y: 2, right: true, left: true, top: true, bottom: true},
        "Ручей безвольных": {x: 4, y: 2, right: true, left: true, top: false, bottom: true},
        "Незримые топи": {x: 5, y: 2, right: true, left: true, top: true, bottom: false},
        "Хижина болотных ведьм": {x: 6, y: 2, right: true, left: true, top: false, bottom: true},
        "Сгоревшая лесопилка": {x: 7, y: 2, right: true, left: true, top: false, bottom: true},
        "Хижина лесничего": {x: 8, y: 2, right: true, left: true, top: true, bottom: false},
        "Сломанный дуб": {x: 9, y: 2, right: true, left: true, top: false, bottom: true},
        "Забытые ворота": {x: 10, y: 2, right: true, left: true, top: true, bottom: true},
        "Лестница темных побуждений": {x: 11, y: 2, right: false, left: true, top: true, bottom: true},
        "Западный тупик": {x: 2, y: 3, right: true, left: false, top: true, bottom: true},
        "Забытый алтарь": {x: 3, y: 3, right: true, left: true, top: true, bottom: true},
        "Лабиринт отступников": {x: 4, y: 3, right: true, left: true, top: true, bottom: true},
        "Проклятая часовня": {x: 5, y: 3, right: true, left: true, top: false, bottom: false},
        "Зловонные каналы": {x: 6, y: 3, right: true, left: true, top: true, bottom: false},
        "Скрытый грот": {x: 7, y: 3, right: true, left: true, top: true, bottom: false},
        "Заброшенный огород": {x: 8, y: 3, right: true, left: true, top: false, bottom: false},
        "Развалины старой колокольни": {x: 9, y: 3, right: true, left: true, top: true, bottom: true},
        "Башня дракона": {x: 10, y: 3, right: true, left: true, top: true, bottom: true},
        "Кузница дьявола": {x: 11, y: 3, right: false, left: true, top: true, bottom: true},
        "Западные захоронения": {x: 4, y: 4, right: true, left: false, top: true, bottom: true},
        "Северная окраина кладбища": {x: 6, y: 4, right: true, left: true, top: false, bottom: true},
        "Северные захоронения": {x: 7, y: 4, right: true, left: true, top: false, bottom: true},
        "Восточная окраина кладбища": {x: 9, y: 4, right: false, left: true, top: true, bottom: true},
        "Овраг мучений": {x: 2, y: 5, right: true, left: false, top: true, bottom: true},
        "Кровавый перекресток": {x: 3, y: 5, right: true, left: true, top: true, bottom: true},
        "Одинокая могила": {x: 4, y: 5, right: false, left: true, top: true, bottom: true},
        "Таинственный склеп": {x: 9, y: 5, right: true, left: false, top: true, bottom: true},
        "Перекресток проклятых": {x: 10, y: 5, right: true, left: true, top: true, bottom: true},
        "Черная заводь": {x: 11, y: 5, right: false, left: true, top: true, bottom: true},
        "Западная окраина кладбища": {x: 4, y: 6, right: true, left: false, top: true, bottom: true},
        "Южные захоронения": {x: 6, y: 6, right: true, left: true, top: true, bottom: false},
        "Южная окраина кладбища": {x: 7, y: 6, right: true, left: true, top: true, bottom: false},
        "Восточные захоронения": {x: 9, y: 6, right: false, left: true, top: true, bottom: true},
        "Утес безумства": {x: 2, y: 7, right: true, left: false, top: true, bottom: true},
        "Дворы повешенных": {x: 3, y: 7, right: true, left: true, top: true, bottom: false},
        "Тоннель оживших мертвецов": {x: 4, y: 7, right: true, left: true, top: true, bottom: true},
        "Бесформенный завал": {x: 5, y: 7, right: true, left: true, top: false, bottom: false},
        "Секретный проход": {x: 6, y: 7, right: true, left: true, top: false, bottom: true},
        "Подворотня слез": {x: 7, y: 7, right: true, left: true, top: false, bottom: true},
        "Сгоревшие конюши": {x: 8, y: 7, right: true, left: true, top: false, bottom: false},
        "Холм висельников": {x: 9, y: 7, right: true, left: true, top: true, bottom: true},
        "Старый сарая": {x: 10, y: 7, right: true, left: true, top: true, bottom: true},
        "Вход в катакомбы": {x: 11, y: 7, right: false, left: true, top: true, bottom: true},
        "Лестница благих намерений": {x: 2, y: 8, right: true, left: false, top: true, bottom: true},
        "Рыночная площадь": {x: 3, y: 8, right: true, left: true, top: false, bottom: true},
        "Подворотня страха": {x: 4, y: 8, right: true, left: true, top: true, bottom: false},
        "Разрушенная казарма": {x: 5, y: 8, right: true, left: true, top: false, bottom: true},
        "Площадь забытых мастеров": {x: 6, y: 8, right: true, left: true, top: true, bottom: false},
        "Заброшенный склад": {x: 7, y: 8, right: true, left: true, top: true, bottom: false},
        "Ратушная площадь": {x: 8, y: 8, right: true, left: true, top: false, bottom: true},
        "Южный тупик": {x: 9, y: 8, right: true, left: true, top: true, bottom: false},
        "Опустувшее хранилище": {x: 10, y: 8, right: true, left: true, top: true, bottom: true},
        "Восточные ворота": {x: 11, y: 8, right: false, left: true, top: true, bottom: true},
        "Синий трон": {x: 1, y: 9, right: true, left: false, top: false, bottom: false},
        "Часовня благих намерений": {x: 2, y: 9, right: true, left: true, top: true, bottom: false},
        "Развалины южных ворот": {x: 3, y: 9, right: true, left: true, top: true, bottom: false},
        "Зловещие провалы": {x: 4, y: 9, right: true, left: true, top: false, bottom: false},
        "Оградительный вал": {x: 5, y: 9, right: true, left: true, top: true, bottom: false},
        "Главные ворота": {x: 6, y: 9, right: true, left: true, top: false, bottom: false},
        "Южные развалины": {x: 7, y: 9, right: true, left: true, top: false, bottom: false},
        "Проклятое место": {x: 8, y: 9, right: true, left: true, top: true, bottom: false},
        "Сумеречныый провал": {x: 9, y: 9, right: true, left: true, top: false, bottom: false},
        "Разрушенная южная башня": {x: 10, y: 9, right: true, left: true, top: true, bottom: false},
        "Бойница стойкости": {x: 11, y: 9, right: true, left: true, top: true, bottom: false},
        "Белая башня": {x: 12, y: 9, right: false, left: true, top: false, bottom: false},
        "Кладбище" :{}
    };

    function generateEmptyTable() {
        var width = 13
        var height = 10
        var tableHtml = '<div id="ruins-table">'
        for (var y = 0; y < height; y++) {
            tableHtml += '<div data-type="row">'
            for (var x = 0; x < width; x++) {
                tableHtml = tableHtml + '<div data-x=' + x + ' data-y=' + y + '></div>'
            }
            tableHtml += '</div>'
        }
        $('#ruins-map').html(tableHtml)
    }

    function createRoutes() {
        locations = []
        for (var obj in locationsObj) {
            locations.push(obj)
            var properties = locationsObj[obj]
            var css_class = '';

            if (properties.right == true) {
                css_class += ' right '
            }

            if (properties.left == true) {
                css_class += ' left '
            }

            if (properties.top == true) {
                css_class += ' top '
            }

            if (properties.bottom == true) {
                css_class += ' bottom '
            }
            var x = properties.x
            var y = properties.y
            var table_item = $('div[data-x=' + x + '][data-y=' + y + ']')

            if (css_class != '') {
                table_item.addClass(css_class)
            }
            var content =
                '<div class="top-border"></div>' +
                    '<div class="left-border"></div>' +
                    '<div class="center"><span>' +
                    '<i class="battle" title="Идет бой" style="position: absolute; right:5px;top:2px;"></i>' +
                    '<i class="trap"  title="Ловушка" style="position: absolute; left:5px;top:2px;"></i>' +
                    '<i class="stop" title="Противник в путах" style="position: absolute; right:15px;top:2px;"></i>' +
                    '</span><a data-target-id=\'' + obj.toLowerCase() + '\'>' +
                    '<i class="icon-bolt"></i>' + obj + '</a></div>' +
                    '<div class = "right-border"></div>' +
                    '<div class = "bottom-border"></div>'

            table_item.html(content)
        }
    }

    function Auto(str) {
        replacer = {
            "q": "й", "w": "ц", "e": "у", "r": "к", "t": "е", "y": "н", "u": "г",
            "i": "ш", "o": "щ", "p": "з", "[": "х", "]": "ъ", "a": "ф", "s": "ы",
            "d": "в", "f": "а", "g": "п", "h": "р", "j": "о", "k": "л", "l": "д",
            ";": "ж", "'": "э", "z": "я", "x": "ч", "c": "с", "v": "м", "b": "и",
            "n": "т", "m": "ь", ",": "б", ".": "ю", "/": "."
        };
        for (i = 0; i < str.length; i++) {
            if (replacer[ str[i].toLowerCase() ] != undefined) {
                if (str[i] == str[i].toLowerCase()) {
                    replace = replacer[ str[i].toLowerCase() ];
                } else if (str[i] == str[i].toUpperCase()) {
                    replace = replacer[ str[i].toLowerCase() ].toUpperCase();
                }
                str = str.replace(str[i], replace);
            }
        }
        return str;

    }

    generateEmptyTable()
    createRoutes()

    $("#location-search").autocomplete({
        delay: 100,
        source: function (request, response) {
            var searchParam = Auto(request.term.toLowerCase())
            function outputItem(item, i, arr) {
                source = item.toLowerCase();
                if (source.indexOf(searchParam) != -1) {
                    return true
                }
                else {
                    return false
                }
            }

            var responseArray = locations.filter(outputItem);
            $('.searched').removeClass('searched')
            $('[data-target-id*=\'' + searchParam + '\']').addClass('searched')
            response(responseArray)
        },
        close: function (event, ui) {
            $('.searched').removeClass('searched')

        },
        select: function (event, ui) {
            var property = ui.item.value;
            var obj = locationsObj[property]
            $('.selected').removeClass('selected')
            var targetObj = $('[data-target-id=\'' + property.toLowerCase() + '\']')
            targetObj.addClass("selected")
            var container = targetObj.closest("[data-x]")
            var x = container.attr('data-x')
            var y = container.attr('data-y')
            $('.header-select').removeClass('header-select')
            $("[data-x='0'][data-y=" + y + "]").addClass('header-select')
            $("[data-x=" + x + "][data-y='0']").addClass('header-select')
        }
    });

    var template_list_item = '<div><a data-target-id=\'{Location}\'>{Name}</a></div>';
    var nav_obj = $('#locations-list')
    locations.forEach(function (value, index) {
        nav_item = template_list_item.replace('{Name}', value)
        nav_item = nav_item.replace('{Location}', value)
        nav_obj.append(nav_item)
    })
    nav_obj.on('hover', 'a', function (e) {
        var target = $(this).attr('data-target-id').toLowerCase()
        $('.selected').removeClass('selected')
        $('[data-target-id=\'' + target + '\']').addClass("selected")


    })

    var additional_objects =
    {
        '&nbsp': {x: 0, y: 0},
        '1': {x: 1, y: 0},
        '2': {x: 2, y: 0},
        '3': {x: 3, y: 0},
        '4': {x: 4, y: 0},
        '5': {x: 5, y: 0},
        '6': {x: 6, y: 0},
        '7': {x: 7, y: 0},
        '8': {x: 8, y: 0},
        '9': {x: 9, y: 0},
        '10': {x: 10, y: 0},
        '11': {x: 11, y: 0},
        '12': {x: 12, y: 0},
        'A': {x: 0, y: 1},
        'Б': {x: 0, y: 2},
        'В': {x: 0, y: 3},
        'Г': {x: 0, y: 4},
        'Д': {x: 0, y: 5},
        'Е': {x: 0, y: 6},
        'Ж': {x: 0, y: 7},
        'З': {x: 0, y: 8},
        'И': {x: 0, y: 9},
        'Кладбище': {x: 6, y: 5}
    }
    for (var add_obj in additional_objects) {
        var properties = additional_objects[add_obj]
        var x = properties.x
        var y = properties.y
        var table_item = $('div[data-x=' + x + '][data-y=' + y + ']')
        var content = '<a data-target-id=\'' + add_obj.toLowerCase() + '\'><i class="icon-bolt"></i>' + add_obj + '</a>'
        table_item.html(content)
    }

});